#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

source $DIR/common.sh

requires "curl"
requires "git"

usage() {
    echo
    echo "USAGE: changelog.sh --buildtag 1.2.3 [--commit]"
    echo "--buildtag X  : provide build tag (version) of build"
    echo "--commit      : commit the changelog to the repository"
    echo "--output X    : output file to write changelog to. Cannot be used with --commit."
}

export CHANGELOG_METHOD="GET"
export DEBUG=""
export OUTPUT_FILE=""

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        --buildtag)
            export BUILDTAG="$2"
            shift
        ;;
        --commit)
            export CHANGELOG_METHOD="POST"
        ;;
        --output)
            export OUTPUT_FILE="$2"
            shift
        ;;
        *)
            usage
            echo "Unknown Option: $key"
            exit 1
        ;;
    esac
    shift
done

if [ -z "$BUILDTAG" ]; then
    echo "Missing --buildtag."
    usage
    exit 1
fi

if [ -z "$CI_PROJECT_ID" ]; then
    echo "Missing 'CI_PROJECT_ID' environment variable."
    usage
    exit 1
fi

if [ -z "$GITLAB_TOKEN" ]; then
    echo "Missing 'GITLAB_TOKEN' environment variable."
    usage
    exit 1
fi

if [ -n "$OUTPUT_FILE" ] && [ "$CHANGELOG_METHOD" == "POST" ]; then
    echo "Error, cannot use --output with --commit."
    usage
    exit 1
fi

echo "Generating changelog for v$BUILDTAG"
echo ""

set -ex

# Generate new changelog

GITLAB_API_BASE="https://gitlab.com/api/v4"
CURL_RESULT_FILE=$(mktemp)

STATUS_CODE=$(curl \
    --write-out "%{http_code}" \
    --output $CURL_RESULT_FILE \
    -X $CHANGELOG_METHOD \
    -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    -d "version=$BUILDTAG&message=%5Bskip%20ci%5D%20Add%20changelog%20for%20version%20${BUILDTAG}" \
    "$GITLAB_API_BASE/projects/${CI_PROJECT_ID}/repository/changelog")
RC=$?
if [[ "$RC" -ne "0" ]]; then
    echo "Error, Unable to generate changelog, error calling GitLab API";
    echo "Error, curl exited with a non-zero code: $RC";
    echo "See docs for more information: https://everything.curl.dev/usingcurl/returns"

    rm -f $CURL_RESULT_FILE
    exit 1
fi
if [[ "$STATUS_CODE" -ne "200" ]]; then
    echo "Error, Unable to create changelog, error calling GitLab API";
    echo "Error, Status Code was $STATUS_CODE, but wanted 200";
    echo "Response body:"
    cat $CURL_RESULT_FILE

    rm -f $CURL_RESULT_FILE
    exit 1
fi

# Check API response for errors: {"error":"message",....}
set +e
grep -q -e '"error"' $CURL_RESULT_FILE
RC=$?
set -e
if [[ "$RC" -eq "0" ]]; then
    echo "Error, Unable to generate changelog, error returned by GitLab API:"
    cat $CURL_RESULT_FILE

    rm -f $CURL_RESULT_FILE
    exit 1
fi

echo "cat CURL_RESULT_FILE"
cat $CURL_RESULT_FILE
if [[ "$CHANGELOG_METHOD" == "GET" ]] && [ -n "$OUTPUT_FILE" ]; then
    mv -f "$CURL_RESULT_FILE" "$OUTPUT_FILE"
else
    rm -f $CURL_RESULT_FILE
fi

# end
