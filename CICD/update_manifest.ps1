# This script gets the new build tag
# and then reads in the VSIX manifest file
# and then updates the Version to be the new build tag
# and finally saves the file.

try {
	$buildtagFile = '.\output\buildtag'
	$buildtag = Get-Content $buildtagFile -Raw -ErrorAction Stop
# Call Resolve-Path to ensure we can save the file at the end
	$manifest = Resolve-Path '.\GitLab.CodeSuggestions\source.extension.vsixmanifest'
	$xml = [xml](Get-Content $manifest)
# TODO This will need to be updated
	$identityId = "gitlab_vs_code_suggestions_poc.383ce117-fc49-41d0-8a35-34b9098a0368"
	$identity = $xml.PackageManifest.Metadata.Identity | where {$_.Id -eq $identityId}
	$buildtag = $buildtag.TrimStart('BUILDTAG=').Trim()
	$identity.Version = $buildtag
	$xml.Save($manifest)
	Write-Output "The Version of $manifest has been updated to $buildtag."
}
catch {
	Write-Output "There was an error updating the source.extension.vsixmanifest. This is an expected failure when not on the main branch."
}
