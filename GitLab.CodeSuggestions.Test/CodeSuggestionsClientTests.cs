﻿using NUnit.Framework;
using System;
using GitLab.CodeSuggestions;
using System.Threading;

namespace GitLab.CodeSuggestions.Test
{
    [TestFixture]
    public class CodeSuggestionsClientTests
    {
        private string GetRequiredEnv(string key)
        {
            var value = Environment.GetEnvironmentVariable(key);
            Assert.NotNull(value, $"Expected non null value for ENV key {key}.");

            return value;
        }

        [Test]
        public void BasicTest()
        {
            var tokenSource = new CancellationTokenSource();

            var client = new CodeSuggestionsClient(
                GetRequiredEnv("GITLAB_TOKEN"));

            var task = client.PostCodeSuggestion(
                "GitLab.CodeSuggestions.Test/file1.cs",
                @"using NUnit.Framework;
using System;
using GitLab.CodeSuggestions;

namespace GitLab.CodeSuggestions.Test
{
    [TestFixture]
    public class CodeSuggestionsClientTests
    {
        [Test]
        public void BasicTest()
        {
            var client = new CodeSuggestionsClient(
                Environment.GetEnvironmentVariable(""GITLAB_SERVER""), 
                Environment.GetEnvironmentVariable(""GITLAB_SERVER""));

            client.PostCodeSuggestion(
                ""GitLab.CodeSuggestions.Test/file1.cs"",
                @""",
                @""",
                @"""");
        }

    }
}", 
                tokenSource.Token);

            var message = task.GetAwaiter().GetResult();

            Assert.IsTrue(message.IsSuccessStatusCode, 
                $"Expected successfull status code, but got {message.StatusCode}.");

            var suggestion = client.SuggestionFromHttpResponseMessage(message);

            Assert.NotNull(suggestion, "Expected a non-null suggestion.");
        }
    }
}
