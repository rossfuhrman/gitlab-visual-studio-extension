## 0.50.0 (2023-09-12)

### changed (1 change)

- [No description](rossfuhrman/gitlab-visual-studio-extension@d418f9de4eae6503d0ba36c94a511534a88fde08) ([merge request](rossfuhrman/gitlab-visual-studio-extension!73))

## 0.49.0 (2023-09-12)

### changed (1 change)

- [wat](rossfuhrman/gitlab-visual-studio-extension@5b159bdc1ac1620d4a9881ce925c59f639ec4b1f) ([merge request](rossfuhrman/gitlab-visual-studio-extension!72))

## 0.48.0 (2023-09-12)

### changed (1 change)

- [blah](rossfuhrman/gitlab-visual-studio-extension@39cfc04600cb422eacff77aed669bbf6cea983e9) ([merge request](rossfuhrman/gitlab-visual-studio-extension!71))

## 0.47.0 (2023-09-12)

### changed (1 change)

- [Try escaping the \'s](rossfuhrman/gitlab-visual-studio-extension@a2d15e1c61ed75648c677090b4c0593f88770f54) ([merge request](rossfuhrman/gitlab-visual-studio-extension!70))

## 0.46.0 (2023-09-12)

### changed (1 change)

- [Remove ##](rossfuhrman/gitlab-visual-studio-extension@f235b16a7d22ed24a2c2ade816b060ef6174fa85) ([merge request](rossfuhrman/gitlab-visual-studio-extension!69))

## 0.45.0 (2023-09-12)

### fixed (1 change)

- [Smoosh](rossfuhrman/gitlab-visual-studio-extension@194f0bfdb4b935fa781732a27cdbb1e4ed5f0ad7) ([merge request](rossfuhrman/gitlab-visual-studio-extension!68))

## 0.44.0 (2023-09-12)

### changed (1 change)

- [test](rossfuhrman/gitlab-visual-studio-extension@23209f7db21bba354208a00e386bb8aef57434ac) ([merge request](rossfuhrman/gitlab-visual-studio-extension!67))

## 0.43.0 (2023-09-12)

### changed (1 change)

- [Extra echo echo](rossfuhrman/gitlab-visual-studio-extension@0f44d709402b312252bec250dfa17df2e797a2f0) ([merge request](rossfuhrman/gitlab-visual-studio-extension!66))

## 0.42.0 (2023-09-12)

### added (1 change)

- [Cat the full file](rossfuhrman/gitlab-visual-studio-extension@e5f2e26857cf5cd225b806100bdda74fada596ae) ([merge request](rossfuhrman/gitlab-visual-studio-extension!65))

## 0.41.0 (2023-09-12)

### changed (1 change)

- [Quicker feedback](rossfuhrman/gitlab-visual-studio-extension@f9eaf1579b5fe5455cde2f4e8bfa7b8e6446548c) ([merge request](rossfuhrman/gitlab-visual-studio-extension!63))

## 0.40.0 (2023-09-12)

### changed (1 change)

- [Try to gen changelog](rossfuhrman/gitlab-visual-studio-extension@35f5c051165132f59e25acdfed30cf69ed7a7770) ([merge request](rossfuhrman/gitlab-visual-studio-extension!62))

## 0.39.0 (2023-09-11)

### changed (1 change)

- [Try using type of "package"](rossfuhrman/gitlab-visual-studio-extension@1ab64ab78bc36c979d2d2f4a3a86328062264d26) ([merge request](rossfuhrman/gitlab-visual-studio-extension!61))

## 0.38.0 (2023-09-08)

### fixed (1 change)

- [Cleanup - it was erroring there](rossfuhrman/gitlab-visual-studio-extension@2474d1418fed2abb226db159a6704a00e12ff8d9) ([merge request](rossfuhrman/gitlab-visual-studio-extension!60))

## 0.37.0 (2023-09-08)

### changed (1 change)

- [Try some things](rossfuhrman/gitlab-visual-studio-extension@bcf691390c24a40c8a3e6c47283e743223b829ed) ([merge request](rossfuhrman/gitlab-visual-studio-extension!59))

## 0.36.0 (2023-09-07)

### changed (1 change)

- [Build url better](rossfuhrman/gitlab-visual-studio-extension@d273fcef49b2df666ebd16833de78633a9668fa8) ([merge request](rossfuhrman/gitlab-visual-studio-extension!58))

## 0.35.0 (2023-09-07)

No changes.

## 0.34.0 (2023-09-07)

### changed (1 change)

- [Using single quote for url, prob won't work](rossfuhrman/gitlab-visual-studio-extension@d70aacc404a6057399254a79ec695301111ab810) ([merge request](rossfuhrman/gitlab-visual-studio-extension!56))

### fixed (1 change)

- [How did that get there?](rossfuhrman/gitlab-visual-studio-extension@8c10dd80029aaf6667ec27d02a97a83802298bf6) ([merge request](rossfuhrman/gitlab-visual-studio-extension!55))

## 0.33.0 (2023-09-07)

### changed (2 changes)

- [Stuff](rossfuhrman/gitlab-visual-studio-extension@603e2dc3f6dd01713e2ce3e356be6b9df83d1488) ([merge request](rossfuhrman/gitlab-visual-studio-extension!54))
- [Filepath has to start with `/`?](rossfuhrman/gitlab-visual-studio-extension@0cc91076356aa2631979d660b791505994a5cca3) ([merge request](rossfuhrman/gitlab-visual-studio-extension!54))

## 0.32.0 (2023-09-07)

No changes.

## 0.31.0 (2023-09-07)

### changed (1 change)

- [Quick test without changelog](rossfuhrman/gitlab-visual-studio-extension@c50cfbe3172473716ff0293b94d6d4d030a1a0a5) ([merge request](rossfuhrman/gitlab-visual-studio-extension!52))

## 0.30.0 (2023-09-07)

### changed (1 change)

- [More tracing](rossfuhrman/gitlab-visual-studio-extension@7dc6f5b810b787b5f0b1206c0d2ccd2baea8eee3) ([merge request](rossfuhrman/gitlab-visual-studio-extension!51))

## 0.29.0 (2023-09-07)

### changed (1 change)

- [A few tweaks](rossfuhrman/gitlab-visual-studio-extension@93395d3c784930cab406be7b6251b2c396f223d9) ([merge request](rossfuhrman/gitlab-visual-studio-extension!50))

## 0.28.0 (2023-09-07)

### added (1 change)

- [Quicker feedback and echos](rossfuhrman/gitlab-visual-studio-extension@e1b28e4b5f3ea2f4a5fb55cad074e20e4aad279d) ([merge request](rossfuhrman/gitlab-visual-studio-extension!49))

## 0.27.0 (2023-09-05)

### changed (2 changes)

- [Does order matter?](rossfuhrman/gitlab-visual-studio-extension@10672cb19bad42239a2d54243a0dd0ab2c13b213) ([merge request](rossfuhrman/gitlab-visual-studio-extension!48))
- [Does order matter?](rossfuhrman/gitlab-visual-studio-extension@f3c8bac805ddad69a82b8514a31f441ecfdcf7e9) ([merge request](rossfuhrman/gitlab-visual-studio-extension!48))

### added (1 change)

- [Attach vsix](rossfuhrman/gitlab-visual-studio-extension@99d5c771d8a823c1148b27aa9c4c3bd351bb703a) ([merge request](rossfuhrman/gitlab-visual-studio-extension!47))

## 0.26.0 (2023-08-31)

No changes.

## 0.25.0 (2023-08-16)

No changes.

## 0.24.0 (2023-08-15)

No changes.

## 0.23.0 (2023-08-15)

No changes.

## 0.22.0 (2023-08-15)

No changes.

## 0.21.0 (2023-06-21)

### changed (1 change)

- [Trying things based on feedback](rossfuhrman/gitlab-visual-studio-extension@db5789cacfbb1e2b79992476425a6dc3bcf32dac) ([merge request](rossfuhrman/gitlab-visual-studio-extension!45))

## 0.20.0 (2023-06-14)

### added (1 change)

- [git pull to ensure we are up to date](rossfuhrman/gitlab-visual-studio-extension@aa3dd666ae3959de313a066cd59e5feb08958944) ([merge request](rossfuhrman/gitlab-visual-studio-extension!44))

### removed (1 change)

- [More deleting](rossfuhrman/gitlab-visual-studio-extension@07a28f6ee114ff0fbd76fd3c7e171c5b475ba22a) ([merge request](rossfuhrman/gitlab-visual-studio-extension!44))

## 0.19.0 (2023-06-14)

### removed (1 change)

- [Some cleanup - mostly removivng comments](rossfuhrman/gitlab-visual-studio-extension@246ffcec5b6c73be90993b17fae9a724a788d47d) ([merge request](rossfuhrman/gitlab-visual-studio-extension!43))

### changed (1 change)

- [Try not specifying origin or main](rossfuhrman/gitlab-visual-studio-extension@b97b920e07c3907cc4de78aa5cfe8ed54ad4ea12) ([merge request](rossfuhrman/gitlab-visual-studio-extension!43))

## 0.18.0 (2023-06-14)

### changed (1 change)

- [Fix path to manifest, turn on full pipeline](rossfuhrman/gitlab-visual-studio-extension@66b4b96688c2fef4af6db83758bfb24264110a47) ([merge request](rossfuhrman/gitlab-visual-studio-extension!42))

## 0.17.0 (2023-06-14)

### added (1 change)

- [Try updating the vsixmanifest](rossfuhrman/gitlab-visual-studio-extension@ed4e9109432b66ff77a4febc64a1db5e40f2204f) ([merge request](rossfuhrman/gitlab-visual-studio-extension!41))

## 0.16.0 (2023-06-14)

### fixed (1 change)

- [Needed the {}](rossfuhrman/gitlab-visual-studio-extension@219b3ec1891d170133f386b8d35003fe9be4d2a2) ([merge request](rossfuhrman/gitlab-visual-studio-extension!40))

### removed (1 change)

- [Hardcode ref to CHANGELOG.MD in Release note](rossfuhrman/gitlab-visual-studio-extension@14d9d3fd92544c3cb64126af0737a66559ad652f) ([merge request](rossfuhrman/gitlab-visual-studio-extension!39))

## 0.15.0 (2023-06-14)

### fixed (1 change)

- [No spaces!](rossfuhrman/gitlab-visual-studio-extension@e82d849064d6b678f9a4925238d29e56aedd5c17) ([merge request](rossfuhrman/gitlab-visual-studio-extension!38))

## 0.14.0 (2023-06-14)

### changed (1 change)

- [Try a few more things](rossfuhrman/gitlab-visual-studio-extension@6117a8708e39eac07c174a27bd03faedef518408) ([merge request](rossfuhrman/gitlab-visual-studio-extension!37))

## 0.13.0 (2023-06-14)

### changed (1 change)

- [Try to fix the release notes](rossfuhrman/gitlab-visual-studio-extension@27ffd8feb5cab4135ded19491cd445190aed7915) ([merge request](rossfuhrman/gitlab-visual-studio-extension!36))

## 0.12.0 (2023-06-13)

### fixed (1 change)

- [I broke puma?](rossfuhrman/gitlab-visual-studio-extension@4a36819a734de7f4e918b02ec474c72763a1a7a7) ([merge request](rossfuhrman/gitlab-visual-studio-extension!35))

## 0.11.0 (2023-06-13)

### changed (1 change)

- [Work on release](rossfuhrman/gitlab-visual-studio-extension@e4fd24b9bb7c07ac8e12c4128d49f5285d78cc10) ([merge request](rossfuhrman/gitlab-visual-studio-extension!34))

## 0.10.0 (2023-06-13)

### changed (1 change)

- [Hard code the fun](rossfuhrman/gitlab-visual-studio-extension@90d64143e54f0a3591a0f79e9877eb4f834c2781) ([merge request](rossfuhrman/gitlab-visual-studio-extension!33))

## 0.9.0 (2023-06-13)

### fixed (1 change)

- [Bunch o fixes](rossfuhrman/gitlab-visual-studio-extension@645cd5e2e3471bf69a24ad72ff9ca27cc61e4c9a) ([merge request](rossfuhrman/gitlab-visual-studio-extension!32))

## 0.8.0 (2023-06-13)

### fixed (1 change)

- [Y A M L](rossfuhrman/gitlab-visual-studio-extension@16ee0d21ec3b4bc305d54e02117bfe8bfe87215a) ([merge request](rossfuhrman/gitlab-visual-studio-extension!31))

### added (2 changes)

- [Need to install the things](rossfuhrman/gitlab-visual-studio-extension@08ac3de04cb0f39eb2379fac6b85e37960714ee8) ([merge request](rossfuhrman/gitlab-visual-studio-extension!31))
- [Save output dir as artifact](rossfuhrman/gitlab-visual-studio-extension@d6fc9049de081cb684ed0bc95a672d47d5a00c61) ([merge request](rossfuhrman/gitlab-visual-studio-extension!31))

## 0.7.0 (2023-06-13)

### changed (2 changes)

- [Quoting!](rossfuhrman/gitlab-visual-studio-extension@690dab872e27823a3451744ffb4bbefdf7a78a36) ([merge request](rossfuhrman/gitlab-visual-studio-extension!30))
- [Another attempt at calling the publisher](rossfuhrman/gitlab-visual-studio-extension@857b9df89e6e22e6a9cafdcd16b567baff6e58b0) ([merge request](rossfuhrman/gitlab-visual-studio-extension!30))

## 0.6.0 (2023-06-13)

### changed (2 changes)

- [Add the file, named correctly](rossfuhrman/gitlab-visual-studio-extension@045676cc27593c540badceedcb6f220a98c7b5c8) ([merge request](rossfuhrman/gitlab-visual-studio-extension!29))
- [Change a bunch](rossfuhrman/gitlab-visual-studio-extension@f67b28b1dc7205ce0a51d19582c44db48562fef4) ([merge request](rossfuhrman/gitlab-visual-studio-extension!29))

## 0.5.0 (2023-06-12)

### added (1 change)

- [Print stuff and try some more powershell](rossfuhrman/gitlab-visual-studio-extension@225e371057066359087c499b440e39500a744d56) ([merge request](rossfuhrman/gitlab-visual-studio-extension!28))

## 0.4.0 (2023-06-12)

No changes.

## 0.3.0 (2023-06-12)

### removed (1 change)

- [Don't worry about PUB tag](rossfuhrman/gitlab-visual-studio-extension@3a667f5626f01f5fc30f67f9231aa1e63dd66115) ([merge request](rossfuhrman/gitlab-visual-studio-extension!26))
