﻿using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Net.Http.Headers;
using System.Windows;
using System;
using System.Data.SqlTypes;

namespace GitLab.CodeSuggestions
{
    public class CodeSuggestionsClient : IAsyncDisposable, IDisposable
    {
        private static HttpClient _httpClient = null;

        private readonly string CompletionsBaseUrl = "https://codesuggestions.gitlab.com";
        private readonly string CompletionsResource = "/v2/completions";
        private readonly string CompletionContentType = "application/json";

        private bool _disposed = false;

        public CodeSuggestionsClient(string accessToken)
        {
            if (_httpClient == null)
                _httpClient = new HttpClient();

            SetAccessToken(accessToken);

            CompletionsResourceUrl = $"{CompletionsBaseUrl}{CompletionsResource}";
        }

        private string CompletionsResourceUrl { get; }

        public void SetAccessToken(string accessToken)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Bearer", accessToken);
        }

        public Task<HttpResponseMessage> PostCodeSuggestion(string filePath, string beforeText, string afterText, CancellationToken token)
        {
            if(_disposed)
                throw new ObjectDisposedException(GetType().FullName);

            // TODO - Make sure content gets disposed at some point
            var content = CreateContent(filePath, beforeText, afterText);
            if(content == null)
            {
#if DEBUG
                MessageBox.Show($"CreateContent returned null.\n\nbeforeText: {beforeText}\n\nafterText: {afterText}", "Error");
#endif
                return Task.FromResult<HttpResponseMessage>(null);
            }

            return _httpClient.PostAsync(CompletionsResourceUrl, content, token);
        }

        public string SuggestionFromHttpResponseMessage(HttpResponseMessage response)
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (!response.IsSuccessStatusCode)
            {
#if DEBUG
                MessageBox.Show(
                    response.Content.ReadAsStringAsync().GetAwaiter().GetResult(), 
                    response.StatusCode.ToString());
#endif
                return null;
            }

            var content = response.Content;
            if (content.Headers.ContentType.MediaType != CompletionContentType)
                return null;

            var body = content.ReadAsStringAsync().GetAwaiter().GetResult();
            
            var root = JObject.Parse(body);
            if (!root.ContainsKey("choices"))
                return null;

            var array = root["choices"] as JArray;
            if (array.Count < 1)
                return null;

            var choice = array[0] as JObject;
            if (!choice.ContainsKey("text"))
                return null;

            return (choice["text"] as JValue).Value<string>();
        }

        private HttpContent CreateContent(string filePath, string beforeText, string afterText)
        {
            if (filePath == null)
                filePath = "";

            if (beforeText == null || afterText == null)
                return null;

            var requestFile = new JObject
            {
                ["file_name"] = filePath,
                ["content_above_cursor"] = new JValue(beforeText),
                ["content_below_cursor"] = new JValue(afterText)
            };

            var root = new JObject
            {
                ["prompt_version"] = new JValue(1),
                ["project_path"] = new JValue(""),
                ["project_id"] = new JValue(-1),
                ["current_file"] = requestFile
            };

            return new StringContent(
                root.ToString(Newtonsoft.Json.Formatting.None), 
                Encoding.UTF8, 
                CompletionContentType);
        }

        public async ValueTask DisposeAsync()
        {
            if (_disposed)
                return;

            // Await the Task, then await the ValueTask
            await await Task.Run<ValueTask>(async () => { Dispose(); return new ValueTask(); });

            // Make sure our this instance is not finalized early
            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _httpClient.Dispose();
            _httpClient = null;
            _disposed = true;

            // Make sure our this instance is not finalized early
            GC.SuppressFinalize(this);
        }
    }
}
