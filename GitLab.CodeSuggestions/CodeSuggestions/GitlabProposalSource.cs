﻿using EnvDTE80;
using GitLab.CodeSuggestions.LanguageServer;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace GitLab.CodeSuggestions.CodeSuggestions
{
    /// <summary>
    /// Generate inline proposals for a unique IWpfTextView instance.
    /// Each open file will have it's own GitlabProposalSource instance.
    /// When the file is closed, the associated GitlabProposalSource instance will be disposed.
    /// </summary>
    internal class GitlabProposalSource : ProposalSourceBase
    {
        private readonly IWpfTextView _textView;
        private readonly string _sourceName = nameof(GitlabProposalSource);

        private CodeSuggestionsClient _codeSuggestionsClient = null;
        private CancellationTokenSource _codeSuggestionsClientCancellationTokenSource;
        private bool _disposed = false;
        private LsProcessManager _lsProcessManager;
        private string _solutionPath;

        public GitlabProposalSource(IWpfTextView textView)
        {
            _textView = textView;
            _codeSuggestionsClientCancellationTokenSource = new CancellationTokenSource();
            _solutionPath = GetSolutionPath();
        }

        /// <summary>
        /// Called by Visual Studio to get a list of proposals from our extension. This
        /// triggers a call to our code suggestions API.
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="scenario"></param>
        /// <param name="triggeringCharacter"></param>
        /// <param name="cancel"></param>
        /// <returns>Returns a GitlabProposalCollection instance or null on error.</returns>
        public override async Task<ProposalCollectionBase> RequestProposalsAsync(
            VirtualSnapshotPoint caret, 
            CompletionState completionState, 
            ProposalScenario scenario, 
            char triggeringCharacter, 
            CancellationToken cancel)
        {
            // Return null to avoid the possiblity of Visual Studio 
            // seeing an exception as a bug and reporting it to the user.
            if (_disposed)
                return null;

            if (!Settings.Instance.Configured)
                return null;

            if(_lsProcessManager == null)
            {
                _lsProcessManager = LsProcessManager.Instance;
                _lsProcessManager.StartLanguageServer(
                    _solutionPath, 
                    Settings.Instance.GitLabAccessToken, 
                    out var port);
            }

            var linkedCancelationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(
                cancel, _codeSuggestionsClientCancellationTokenSource.Token);

            var suggestion = await GetCodeSuggestion(linkedCancelationTokenSource.Token);
            if (suggestion == null || cancel.IsCancellationRequested)
                return null;

            return CreateProposalCollection(caret, completionState, suggestion);
        }

        /// <summary>
        /// Called when a code suggestion has been accepted to commit the change to the buffer.
        /// </summary>
        /// <param name="edits"></param>
        private void CommitEdits(IReadOnlyList<ProposedEdit> edits)
        {
            // todo -- add metric for accepted suggestions

            foreach (var edit in edits)
            {
                SnapshotPoint? caretPoint = _textView.Caret.Position.Point.GetPoint(
                    textBuffer => (!textBuffer.ContentType.IsOfType("projection")), PositionAffinity.Predecessor);

                if (!caretPoint.HasValue)
                    continue;

                var snapshot = _textView.TextBuffer.Insert(_textView.Caret.Position.BufferPosition, edit.ReplacementText);
            }
        }

        /// <summary>
        /// Create a GitlabProposalCollection from a code suggestion
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="suggestion"></param>
        /// <returns></returns>
        private ProposalCollection CreateProposalCollection(
            VirtualSnapshotPoint caret,
            CompletionState completionState, 
            string suggestion)
        {
            // Create proposals
            var proposalCaret = _textView.Caret.Position.VirtualBufferPosition;
            var flags = ProposalFlags.FormatAfterCommit | ProposalFlags.MoveCaretToEnd;

            var edits = new List<ProposedEdit>
            {
                new ProposedEdit(
                    _textView.GetTextElementSpan(_textView.Caret.Position.BufferPosition),
                    suggestion)
            };

            var proposals = new List<Proposal> {
                new Proposal(null, edits, caret, completionState, flags,
                    () => { CommitEdits(edits); return true; },
                    null, suggestion)
            };

            return new ProposalCollection(_sourceName, proposals);
        }

        /// <summary>
        /// Call the Code Suggestion API and return a suggestion.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Returns a suggestion or null</returns>
        private async Task<string> GetCodeSuggestion(CancellationToken cancellationToken)
        {
            if (_codeSuggestionsClient == null)
            {
                _codeSuggestionsClient = new CodeSuggestionsClient(
                    Settings.Instance.GitLabAccessToken);
            }

            if (!GetBeforeAfterString(out var beforeText, out var afterText))
                return null;

            // Do this everytime in case the user has updated their settings
            _codeSuggestionsClient.SetAccessToken(Settings.Instance.GitLabAccessToken);

            var response = await _codeSuggestionsClient.PostCodeSuggestion(
                GetFilename(), beforeText, afterText, cancellationToken);
            if (response == null)
                return null;

            var suggestion = _codeSuggestionsClient.SuggestionFromHttpResponseMessage(response);
            if (suggestion == null)
                return null;

            return suggestion;
        }

        /// <summary>
        /// Get the file name + path being editted. 
        /// The solutions path prefix is removed from the filename.
        /// </summary>
        /// <returns></returns>
        private string GetFilename()
        {
            var uielement = _textView as System.Windows.UIElement;
            var filepath = string.Empty;

            uielement.Dispatcher.Invoke(new Action(() =>
            {
                Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();

                _textView.TextBuffer.Properties.TryGetProperty<IVsTextBuffer>(typeof(IVsTextBuffer), out var bufferAdaptor);
                if (bufferAdaptor == null)
                    return;

                if (!(bufferAdaptor is IPersistFileFormat persistFileFormat))
                    return;

                persistFileFormat.GetCurFile(out filepath, out _);

                var pathPrefix = GetContainingSolutionPathPrefix(filepath);

                if (pathPrefix != null 
                    && filepath.StartsWith(pathPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    filepath = filepath.Substring(pathPrefix.Length);
                    filepath = filepath.TrimStart(Path.DirectorySeparatorChar);
                }
            }));

            if (string.IsNullOrEmpty(filepath))
                return null;

            return filepath;
        }

        /// <summary>
        /// Get solution path.
        /// </summary>
        /// <returns>Solution path or null if error occured.</returns>
        private string GetSolutionPath()
        {
            var uielement = _textView as System.Windows.UIElement;
            string solutionPath = null;

            uielement.Dispatcher.Invoke(new Action(() =>
            {
                Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();

                var dte2 = (DTE2)Package.GetGlobalService(typeof(SDTE));
                if (dte2 == null)
                    return;

                solutionPath = dte2.Solution.FileName;
            }));

            return solutionPath;
        }

        /// <summary>
        /// Get the solutions path prefix. The solution folder
        /// name is left in, so all files have at least one directory in their path.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>Returns a path prefix or null.</returns>
        private string GetContainingSolutionPathPrefix(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return null;

            // Remove the last directory from the path
            var pathPrefix = Path.GetDirectoryName(_solutionPath);
            return pathPrefix;
        }

        /// <summary>
        /// Get the buffer content before and after the current cursor position.
        /// </summary>
        /// <param name="beforeText"></param>
        /// <param name="afterText"></param>
        /// <returns></returns>
        private bool GetBeforeAfterString(out string beforeText, out string afterText)
        {
            beforeText = string.Empty;
            afterText = string.Empty;

            var caretPoint = _textView.Caret.Position.Point.GetPoint(
                textBuffer => (!textBuffer.ContentType.IsOfType("projection")), PositionAffinity.Predecessor);

            if (!caretPoint.HasValue)
                return false;

            beforeText = _textView.TextBuffer.CurrentSnapshot.GetText(
                0,
                _textView.Caret.Position.BufferPosition);
            afterText = _textView.TextBuffer.CurrentSnapshot.GetText(
                _textView.Caret.Position.BufferPosition,
                _textView.TextBuffer.CurrentSnapshot.Length - _textView.Caret.Position.BufferPosition);

            return true;
        }

        public override async Task DisposeAsync()
        {
            if (_disposed)
                return;

            // Cancel any pending requests
            if(_codeSuggestionsClientCancellationTokenSource != null)
                _codeSuggestionsClientCancellationTokenSource.Cancel();

            // Dispose our API client
            if(_codeSuggestionsClient != null)
                await _codeSuggestionsClient.DisposeAsync();

            _codeSuggestionsClient = null;
            _codeSuggestionsClientCancellationTokenSource = null;

            if (_lsProcessManager != null)
                await _lsProcessManager.StopLanguageServerAsync(_solutionPath);

            _lsProcessManager = null;

            _disposed = true;
        }
    }
}
