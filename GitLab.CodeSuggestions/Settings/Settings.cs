﻿using Microsoft.VisualStudio.Settings;
using Microsoft.VisualStudio.Shell.Settings;
using Microsoft.VisualStudio.Shell;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Collections.ObjectModel;
using System;
using Microsoft.Win32;

namespace GitLab.CodeSuggestions
{
    public class Settings
    {
        private const string ApplicationName = "GitLabCodeSuggestionsVisualStudio";
        private const string GitLabServerKey = "GitLabServer";
        private const string GitLabAccessTokenKey = "GitLabAccessToken";

        private static Settings _instance = null;

        private WritableSettingsStore _settingsStore = null;
        private string _gitlabServer = null;
        private string _gitlabAccessToken = null;
        private IServiceProvider _serviceProvider = null;

        private Settings()
        {
            LoadRegistry();
        }

        public static Settings Instance
        {
            get
            {
                if(_instance == null)
                    _instance = new Settings();

                return _instance;
            }
        }

        public string GitLabServer
        {
            get { return _gitlabServer; }
            set
            {
                _gitlabServer = value;
                SaveRegistry();
            }
        }

        public string GitLabAccessToken
        {
            get { return _gitlabAccessToken; }
            set
            {
                _gitlabAccessToken = value;
                SaveRegistry();
            }
        }

        public IServiceProvider ServiceProvider
        {
            get
            {
                return _serviceProvider;
            }

            set
            {
                _serviceProvider = value;

                ThreadHelper.ThrowIfNotOnUIThread();
                var settingsManager = new ShellSettingsManager(_serviceProvider);
                _settingsStore = settingsManager.GetWritableSettingsStore(SettingsScope.UserSettings);

                //Load();

            }
        }

        /// <summary>
        /// Do we have a valid looking configuration
        /// </summary>
        public bool Configured
        {
            get
            {
                return !(string.IsNullOrEmpty(GitLabAccessToken));
            }
        }

        public void Save()
        {
            if (_settingsStore == null)
                return;

            ThreadHelper.ThrowIfNotOnUIThread();

            _settingsStore.CreateCollection(ApplicationName);

            _settingsStore.SetString(ApplicationName, GitLabServerKey, _gitlabServer);
            _settingsStore.SetString(ApplicationName, Protect(GitLabAccessTokenKey), _gitlabAccessToken);
        }

        public void Load()
        {
            if (_settingsStore == null)
                return;

            ThreadHelper.ThrowIfNotOnUIThread();

            _settingsStore.CreateCollection(ApplicationName);

            _gitlabServer = _settingsStore.GetString(ApplicationName, GitLabServerKey, string.Empty);
            var protectedAccessToken = _settingsStore.GetString(ApplicationName, GitLabAccessTokenKey, null);
            if (!string.IsNullOrEmpty(protectedAccessToken))
            {
                _gitlabAccessToken = Unprotect(protectedAccessToken);
            }
            else
            {
                _gitlabAccessToken = string.Empty;
            }
        }
        public void LoadRegistry()
        {
            var key = Registry.CurrentUser.OpenSubKey($"Software\\{ApplicationName}", true);
            if (key == null)
            {
                _gitlabServer = string.Empty;
                _gitlabAccessToken = string.Empty;

                return;
            }

            try
            {
                _gitlabServer = (string)key.GetValue(GitLabServerKey, string.Empty);
                var protectedAccessToken = (string)key.GetValue(GitLabAccessTokenKey, null);
                if (!string.IsNullOrEmpty(protectedAccessToken))
                {
                    _gitlabAccessToken = Unprotect(protectedAccessToken);
                }
                else
                {
                    _gitlabAccessToken = string.Empty;
                }
            }
            finally
            {
                key.Dispose();
            }
        }

        public void SaveRegistry()
        {
            var key = Registry.CurrentUser.OpenSubKey($"Software\\{ApplicationName}", true);
            if (key == null)
            {
                using (var softwareKey = Registry.CurrentUser.OpenSubKey($"Software", true))
                { 
                    key = softwareKey.CreateSubKey(ApplicationName);
                }
            }

            try
            {
                key.SetValue(GitLabServerKey, _gitlabServer, RegistryValueKind.String);
                key.SetValue(GitLabAccessTokenKey, Protect(_gitlabAccessToken), RegistryValueKind.String);
            }
            finally
            {
                key.Dispose();
            }
        }

        private static string Protect(string data)
        {
            // TODO -- How should we handle this error?
            try
            {
                var dataAsBytes = UTF8Encoding.UTF8.GetBytes(data);
                var protectedAsBytes = ProtectedData.Protect(dataAsBytes, null, DataProtectionScope.CurrentUser);
                return System.Convert.ToBase64String(protectedAsBytes);
            }
            catch (CryptographicException e)
            {
                MessageBox.Show(e.ToString(), "Error protecting data");
                return data;
            }
        }

        private static string Unprotect(string protectedData)
        {
            // TODO -- How should we handle this error?
            try
            {
                var protectedDataAsBytes = System.Convert.FromBase64String(protectedData);
                var dataAsBytes = ProtectedData.Unprotect(protectedDataAsBytes, null, DataProtectionScope.CurrentUser);
                return UTF8Encoding.UTF8.GetString(dataAsBytes);
            }
            catch (CryptographicException e)
            {
                MessageBox.Show(e.ToString(), "Error unprotecting data");
                return protectedData;
            }
        }
    }
}
