﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Threading;
using Task = System.Threading.Tasks.Task;
using System.ComponentModel;

namespace GitLab.CodeSuggestions
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the
    /// IVsPackage interface and uses the registration attributes defined in the framework to
    /// register itself and its components with the shell. These attributes tell the pkgdef creation
    /// utility what data to put into .pkgdef file.
    /// </para>
    /// <para>
    /// To get loaded into VS, the package must be referred by &lt;Asset Type="Microsoft.VisualStudio.VsPackage" ...&gt; in .vsixmanifest file.
    /// </para>
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(GitLabOptionsPackage.PackageGuidString)]
    [ProvideOptionPage(typeof(OptionPageGrid), "GitLab", "General", 0, 0, true)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    public sealed class GitLabOptionsPackage : AsyncPackage
    {
        /// <summary>
        /// MyToolsOptionsPackage GUID string.
        /// </summary>
        public const string PackageGuidString = "8fc714ec-e5ad-47d0-99a2-9545ecb7923f";

        /// <summary>
        /// Initializes a new instance of the <see cref="GitLabOptionsPackage"/> class.
        /// </summary>
        public GitLabOptionsPackage()
        {
            // Inside this method you can place any initialization code that does not require
            // any Visual Studio service because at this point the package object is created but
            // not sited yet inside Visual Studio environment. The place to do all the other
            // initialization is the Initialize method.
        }

        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token to monitor for initialization cancellation, which can occur when VS is shutting down.</param>
        /// <param name="progress">A provider for progress updates.</param>
        /// <returns>A task representing the async work of package initialization, or an already completed task if there is none. Do not return null from this method.</returns>
        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            // When initialized asynchronously, the current thread may be a background thread at this point.
            // Do any initialization that requires the UI thread after switching to the UI thread.
            await this.JoinableTaskFactory.SwitchToMainThreadAsync(cancellationToken);
        }

        #endregion
    }

    public class OptionPageGrid : DialogPage
    {
        //private bool seenFirstSetGitLabInstance = false;
        private bool seenFirstSetGitLabAccessToken = false;

        //[Category("GitLab")]
        //[DisplayName("GitLab Instance")]
        //[Description("URL to GitLab Instance. Exmaple: https://gitlab.com")]
        //public string GitLabInstance
        //{
        //    get { return Settings.Instance.GitLabServer; }
        //    set
        //    {
        //        // Skip the first set as it's VS pusshing it's data on us
        //        // we are hacking around things to use the registry instead of
        //        // VS settings store.
        //        if (seenFirstSetGitLabInstance)
        //            Settings.Instance.GitLabServer = value;
        //        else
        //            seenFirstSetGitLabInstance = true;
        //    }
        //}

        [Category("GitLab")]
        [DisplayName("Access Token")]
        [PasswordPropertyText(true)]
        [Description("Personal access token with read_api and read_user permissions.")]
        public string AccessToken
        {
            get { return Settings.Instance.GitLabAccessToken; }
            set
            {
                // Skip the first set as it's VS pusshing it's data on us
                // we are hacking around things to use the registry instead of
                // VS settings store.
                if (seenFirstSetGitLabAccessToken)
                    Settings.Instance.GitLabAccessToken = value;
                else
                    seenFirstSetGitLabAccessToken = true;
            }
        }
    }
}
