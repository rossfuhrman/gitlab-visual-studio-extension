﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Threading;
using System.Collections.Concurrent;

namespace GitLab.CodeSuggestions.LanguageServer
{
    /// <summary>
    /// Manage language server processes. 
    /// Singleton pattern with single instance as LsProcessManager.Instance.
    /// </summary>
    internal class LsProcessManager : IAsyncDisposable
    {
        /// <summary>
        /// All language server processes. Key is Solution Path.
        /// </summary>
        private static ConcurrentDictionary<string, Process> Processes = new ConcurrentDictionary<string, Process>();
        /// <summary>
        /// Locks for language server process. Key is Solution Path.
        /// </summary>
        private static ConcurrentDictionary<string, 
            (object lockLanguageServer, SpinLock stoppingLanguageServer)> ProcessLocks = new ConcurrentDictionary<string, (object lockLanguageServer, SpinLock stoppingLanguageServer)>();
        /// <summary>
        /// Port language server is listening on. Key is Solution Path.
        /// </summary>
        private static ConcurrentDictionary<string, int> LanguageServerPorts = new ConcurrentDictionary<string, int>();

        private const string LsExecutable = "gitlab-code-suggestions-language-server-windows-amd64.exe";

        /// <summary>
        /// Singleton instance of LsProcessManager
        /// </summary>
        public static LsProcessManager Instance = new LsProcessManager();

        private bool _disposed;

        private LsProcessManager()
        {
        }

        /// <summary>
        /// Finalizer to make sure we cleanup all of our resources
        /// </summary>
        ~LsProcessManager()
        {
            DisposeAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// Start a language server watching solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <param name="gitlabToken"></param>
        /// <param name="port">Port the server is listening on</param>
        /// <returns>True if a language server was started, false on error, or if the server is already started.
        /// If the language server was already started, port is set to the listing port.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        public bool StartLanguageServer(string solutionPath, string gitlabToken, out int port)
        {
            if(_disposed)
            {
                throw new ObjectDisposedException(nameof(LsProcessManager));
            }

            if (!ProcessLocks.TryGetValue(solutionPath, out var locks))
            {
                locks = (new object(), new SpinLock());

                if(!ProcessLocks.TryAdd(solutionPath, locks))
                    ProcessLocks.TryGetValue(solutionPath, out locks);
            }

            lock (locks.lockLanguageServer)
            {
                if (!Processes.TryGetValue(solutionPath, out var lsProcess))
                {
                    lsProcess = new Process();

                    if (!Processes.TryAdd(solutionPath, lsProcess))
                        Processes.TryGetValue(solutionPath, out lsProcess);
                }
                else
                {
                    if(!LanguageServerPorts.TryGetValue(solutionPath, out port))
                    {
                        // This should never happen
                        throw new ApplicationException("LangugageServerPorts.TryGetValue failed. LsProcessManager dictionaries in unknown state.");
                    }

                    return false;
                }

                var lsPath = GetLsExecutablePath();
                port = GetAvailablePort();
                LanguageServerPorts.TryAdd(solutionPath, port);

                var processStartInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    FileName = lsPath,
                    Arguments = "serve"
                };
                processStartInfo.Environment.Add("LANGSRV_NAME", solutionPath);
                processStartInfo.Environment.Add("LANGSRV_SRC_DIR", solutionPath);
                processStartInfo.Environment.Add("LANGSRV_GITLAB_API_TOKEN", gitlabToken);
                processStartInfo.Environment.Add("LANGSRV_HOST", IPAddress.Loopback.ToString());
                processStartInfo.Environment.Add("LANGSRV_PORT", port.ToString());

                lsProcess.StartInfo = processStartInfo;
                lsProcess.Exited += _lsProcess_Exited;
                lsProcess.ErrorDataReceived += _lsProcess_ErrorDataReceived;
                lsProcess.OutputDataReceived += _lsProcess_OutputDataReceived;
                lsProcess.Start();

                try
                {
                    // Link our new child process to the visual studio process.
                    // This will guarantee that killing visual studio will also
                    // kill our children.
                    ChildProcessTracker.AddProcess(lsProcess);
                }
                catch
                {
                    // Ignore exceptions.
                    // If visual studio is running in compatibility mode with Win7,
                    // we might get an exception. Nothing we can do if an exception
                    // is raised.
                }

                return true;
            }
        }

        private void _lsProcess_Exited(object sender, EventArgs e)
        {
            var locks = ProcessLocks[(sender as Process).StartInfo.Environment["LANGSRV_SRD_DIR"]];

            if (locks.stoppingLanguageServer.IsHeld)
                return;

            // TODO - Restart server on early exit
            //      - Need to prevent quick restarts. An exponetial back off
            //      - and max retry is needed.
        }

        private void _lsProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            var solutionPath = (sender as Process).StartInfo.Environment["LANGSRV_SRD_DIR"];
            if (!LanguageServerPorts.TryGetValue(solutionPath, out var lsPort))
                lsPort = -1;

            foreach (var line in e.Data.SplitToLines())
            {
                Debug.WriteLine($"LS({lsPort}): {line}");
            }
        }

        private void _lsProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            var solutionPath = (sender as Process).StartInfo.Environment["LANGSRV_SRD_DIR"];
            if (!LanguageServerPorts.TryGetValue(solutionPath, out var lsPort))
                lsPort = -1;

            foreach (var line in e.Data.SplitToLines())
            {
                Debug.WriteLine($"LS({lsPort}): {line}");
            }
        }

        /// <summary>
        /// Stop the language server associated with solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns true if a server was killed, false otherwise.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        public async Task<bool> StopLanguageServerAsync(string solutionPath)
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(LsProcessManager));
            }

            if (!ProcessLocks.TryGetValue(solutionPath, out var locks))
                return false;

            return await Task.Run<bool>(new Func<bool>(() =>
            {
                lock (locks.lockLanguageServer)
                {
                    var lockToken = false;
                    locks.stoppingLanguageServer.Enter(ref lockToken);
                    if (!lockToken)
                        return false;

                    try
                    {
                        var lsProcess = Processes[solutionPath];

                        if (lsProcess == null)
                            return true;

                        lsProcess.Kill();
                        lsProcess.WaitForExit();
                        lsProcess.Dispose();
                        lsProcess = null;

                        LanguageServerPorts.TryRemove(solutionPath, out _);
                        Processes.TryRemove(solutionPath, out _);
                        ProcessLocks.TryRemove(solutionPath, out _);

                        return true;
                    }
                    finally
                    {
                        locks.stoppingLanguageServer.Exit();
                    }
                }
            }));
        }

        private string GetLsExecutablePath()
        {
            var extensionPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var lsPath = Path.Combine(extensionPath, "Resources");
            lsPath = Path.Combine(lsPath, LsExecutable);

#if DEBUG
            // TODO convert this into a log statement
            if (!File.Exists(lsPath))
                MessageBox.Show($"executable not found in path.\n{extensionPath}");
#endif

            return lsPath;
        }

        /// <summary>
        /// Get an available (free) TCP port
        /// </summary>
        /// <returns>Returns an available (free) TCP port</returns>
        private int GetAvailablePort()
        {
            var loopbackEndpoint = new IPEndPoint(IPAddress.Loopback, port: 0);
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Bind(loopbackEndpoint);
                return ((IPEndPoint)socket.LocalEndPoint).Port;
            }
        }

        protected async Task DisposeAsync(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                foreach(var solution in Processes.Keys.ToArray())
                {
                    await StopLanguageServerAsync(solution);
                }
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            _disposed = true;
        }

        /// <summary>
        /// Dispose of heald resources.
        /// </summary>
        /// <param name="solutionPath">Dispose of just resources related to solutionPath. When null, all resources are freed</param>
        /// <returns></returns>
        public async ValueTask DisposeAsync()
        {
            await DisposeAsync(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
