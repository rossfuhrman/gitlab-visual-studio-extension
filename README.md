## Visual Studio IDE Code Suggestions

This is a code suggestions extension for Visual Studio (Community/Pro/Enterprise).

### Developer Environment

_Required Software:_

* Virtual Machine Software
  * Longer term: Parallels (Paid)
  * Short term: VirtualBox (may not work on M1/M2)
* Windows 10/11 (Paid)
* Visual Studio Professional (Paid)
  * Make sure to select Visual Studio Extensions component during installation
* [.NET Framework 4.7.2 Developer Pack](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472)

_Recommended Extras:_

* [Cmder console replacement](https://cmder.app/)
  * Comes with `git`
* [LINQPad](https://www.linqpad.net/)

_After cloning the project:_

1. Create a `.runsettings` file with the gitlab url and access token
```xml
<?xml version="1.0" encoding="utf-8"?>
<RunSettings>
    <RunConfiguration>
        <EnvironmentVariables>
            <GITLAB_SERVER>https://gitlab.com</GITLAB_SERVER>
            <GITLAB_TOKEN>glpat-XXXX</GITLAB_TOKEN>
        </EnvironmentVariables>
    </RunConfiguration>
</RunSettings>
```

### Testing

#### Environment

* VirtualBox (Free)
* Windows 10/11 (Paid)
* Visual Studio Community (Free)

#### How to install

1. Download the extension archive
1. Extract archive
1. Double click on the file ending in `.vsix`. This will start the extension installation process.
